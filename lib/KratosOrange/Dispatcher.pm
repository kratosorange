package KratosOrange::Dispatcher;
use strict;
use warnings;
use Jifty::Dispatcher -base;

before '*' => run {
    if (Jifty->web->current_user->id) {
        # authenticated user, show options
        my $top = Jifty->web->navigation;
        $top->child( 'Home'        => url => '/' );
        $top->child( 'My workouts' => url => '/workouts' );
        $top->child( 'Preferences' => url => '/prefs' );
        $top->child( 'Logout'      => url => '/logout' );
    }
    elsif ($1 !~ /^login|^signup/) {
        tangent 'login';
    }
};

on '/' => run {
    my $emped_id = Jifty->web->current_user->user_object->empedid;
    Jifty->log->debug('empedid: ' . $emped_id);
    if (! $emped_id) {
        # user has not set his emped_id yet, send him off to preferences
        tangent '/prefs';
    }
    my @new_files
        = glob("/Volumes/*/iPod_Control/Device/Trainer/Workouts/Empeds/$emped_id/latest/*.xml");
    my @old_files
        = glob("/Volumes/*/iPod_Control/Device/Trainer/Workouts/Empeds/$emped_id/synched/*.xml");
    Jifty->log->debug('new files: ' . join q{, }, @new_files);
    Jifty->log->debug('old files: ' . join q{, }, @old_files);

    my $workouts = KratosOrange::Model::WorkoutCollection->new();
    $workouts->limit(
        column => 'user_info_empedid',
        value  => $emped_id,
    );
    if (! $workouts->count && scalar @old_files) {
        # this user has no workouts recorded, let him import ones
        # already given away
        tangent '/import_synched';
    }

    # if new files are available, import them
    foreach my $file (@new_files) {
        my $workout = KratosOrange::Model::Workout->new_from_XML($file);
        # move file to 'synched' folder
        my $new_location = $file;
        $new_location =~ s/latest/synched/g;
        rename($file, $new_location);
    }

    show 'summary';
};

on qr{ \A /workout/(\d+) \z }xms => run {
    my $workout_id = $1;
    set 'workout_id' => $workout_id;
    show 'workout';
};

on qr{ \A /workout/(\d+)/chart \z }xms => run {
    my $workout_id = $1;
    set 'workout_id' => $workout_id;
    show 'chart';
};

on qr{ \A /workout/(\d+)/edit \z }xms => run {
    my $workout_id = $1;
    set 'workout_id' => $workout_id;
    show 'workout_edit';
};

on '/workouts'       => show 'workouts';
on '/prefs'          => show 'preferences';
on '/import_synched' => show 'import_synched';

1;
