use strict;
use warnings;

package KratosOrange::Record;

use base qw(Jifty::Record);

# FIXME - figure out how to do ACL stuff properly ...
sub check_read_rights { return 1 }

sub check_update_rights { return 1 }

sub check_create_rights { return 1 }
1;
