use strict;
use warnings;

=head1 NAME

KratosOrange::Action::ImportSynched

=cut

package KratosOrange::Action::ImportSynched;
use base qw/KratosOrange::Action Jifty::Action/;

use Jifty::Param::Schema;
use Jifty::Action schema {
    param import =>
        type is 'checkbox',
        label is 'Import already synchronized data',
        default is 1;
};

=head2 take_action

=cut

sub take_action {
    my $self = shift;
    
    # TODO - only do this if it is wanted ...
    my $emped_id = Jifty->web->current_user->user_object()->empedid();
    my @files = glob("/Volumes/*/iPod_Control/Device/Trainer/Workouts/Empeds/$emped_id/synched/*.xml");
    Jifty->log->debug('files: ' . join q{, }, @files);
    foreach my $file (@files) {
        my $workout = KratosOrange::Model::Workout->new_from_XML($file);
    }
    
    $self->report_success if not $self->result->failure;
    
    return 1;
}

=head2 report_success

=cut

sub report_success {
    my $self = shift;
    # Your success message here
    $self->result->message('Successfully imported runs');
}

1;

