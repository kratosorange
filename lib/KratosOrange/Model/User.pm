use strict;
use warnings;

package KratosOrange::Model::User;
use Jifty::DBI::Schema;

use KratosOrange::Record schema {
    column empedid =>
        label is 'Sensor ID',
};

use Jifty::Plugin::User::Mixin::Model::User;
use Jifty::Plugin::Authentication::Password::Mixin::Model::User;

sub get_emped_ids_from_fs {
    my $self = shift;

    # TODO - figure out why glob does not work here ...
    #my @locations = glob('/Volumes/*/iPod_Control/Device/Trainer/Workouts/Empeds/*');
    my @ids = split(/\n/, `ls -d /Volumes/*/iPod_Control/Device/Trainer/Workouts/Empeds/*/preferences.xml`);
    map { s{ .* /([A-Z0-9]+)/preferences.xml }{$1}xms } @ids;
    Jifty->log->debug("Emped IDs: " . join(q{, }, @ids));
    return @ids;
}

1;

