use strict;
use warnings;

package KratosOrange::Model::WayPoint;
use Jifty::DBI::Schema;
use Chart::Clicker::Renderer::Area;
use Chart::Clicker;
use Chart::Clicker::Axis;
use Chart::Clicker::Axis::DateTime;
use Chart::Clicker::Data::DataSet;
use Chart::Clicker::Data::Series;
use Chart::Clicker::Decoration::Grid;
use Chart::Clicker::Decoration::Label;
use Chart::Clicker::Decoration::Legend;
use Chart::Clicker::Decoration::Plot;
use Chart::Clicker::Drawing qw(:positions);
use Chart::Clicker::Drawing::Insets;
use Chart::Clicker::Drawing::Color;
use Chart::Clicker::Drawing::ColorAllocator;

use KratosOrange::Record schema {
    column workout_id =>
        refers_to KratosOrange::Model::Workout;

    column serial =>
        type is 'integer';

    # TODO - use something akin to float?
    column distance =>
        type is 'varchar';
};

# Your model-specific methods go here.

sub new_from_extended_data {
    my $self = shift;
    my $extended_data = shift; # a string as found in the XML file
    my $workout_id    = shift; # references the corresponding workout
    my @points = split(q{, }, $extended_data);
    for (my $i = 0; $i < scalar @points; $i++) {
        my $way_point = KratosOrange::Model::WayPoint->new();
        $way_point->create();
        $way_point->set_workout_id($workout_id);
        $way_point->set_serial($i);
        $way_point->set_distance($points[$i]);
    }

    return 1;
}

sub chart_from_workout {
    my $self       = shift;
    my $workout_id = shift;
    my @values     = ();
    my @keys       = ();

    my $workout    = KratosOrange::Model::Workout->load($workout_id);
    my $points     = KratosOrange::Model::WayPointCollection->new();
    $points->limit(
        column => 'workout_id',
        value  => $workout_id,
    );

    if ($workout->interval_unit() ne 's') {
        die "Can not compute with non-seconds";
    }

    my $curr_key = 0;
    my $old = $points->next();
    while (my $new = $points->next()) {
        # compute average speed in km/h between two waypoints
        my $delta_distance = $new->distance - $old->distance;
        my $calib_factor
            = $workout->software_calibration ? $workout->software_calibration : 1;
        push @values,
            (3600 / $workout->interval_value) * $delta_distance * $calib_factor;
        push @keys,   $curr_key;

        $curr_key += $workout->interval_value;
        $old = $new;
    }
    my $renderer = Chart::Clicker::Renderer::Area->new();
    $renderer->options({
            opacity => .8,
    });
    my $series = Chart::Clicker::Data::Series->new({
        keys   => \@keys,
        values => \@values,
    });
    my $chart = Chart::Clicker->new({
        datasets => [
            Chart::Clicker::Data::DataSet->new({
                series => [ $series ],
            }),
        ],
        width => 700,
        height => 250,
    });
    $chart->color_allocator(
        Chart::Clicker::Drawing::ColorAllocator->new({
            colors => [
                Chart::Clicker::Drawing::Color->new({
                        red   => 0,
                        blue  => 0.7,
                        green => 0,
                 }),
            ],
        }),
    );
    my $daxis = Chart::Clicker::Axis->new({
        orientation => $CC_HORIZONTAL,
        position    => $CC_BOTTOM,
        format      => '%02d',
        label       => 'Time (s)',
    });
    $chart->add($daxis, $CC_AXIS_BOTTOM);

    # Range axis
    my $raxis = Chart::Clicker::Axis->new({
        orientation => $CC_VERTICAL,
        position    => $CC_LEFT,
        format      => '%0.2f',
        label       => 'Speed (km/h)',
    });
    $chart->add($raxis, $CC_AXIS_LEFT);

    $chart->range_axes([ $raxis ]);
    $chart->domain_axes([ $daxis ]);

    $chart->add(Chart::Clicker::Decoration::Grid->new(), $CC_CENTER, 0);

    my $plot = Chart::Clicker::Decoration::Plot->new();
    $plot->renderers([ $renderer ]);
    $chart->add($plot, $CC_CENTER);
    $plot->set_renderer_for_dataset(1, 1);

    $chart->prepare();
    $chart->draw();

    return $chart->png();
}
1;

