use strict;
use warnings;

package KratosOrange::Model::Workout;
use Jifty::DBI::Schema;
use KratosOrange::Model::WayPoint;

use XML::Simple;
use Data::Dumper;

use KratosOrange::Record schema {
    column distance_string =>
        type is 'varchar',
        label is 'Distance';

    column duration_string =>
        type is 'varchar',
        label is 'Duration';

    column distance_unit =>
        type is 'varchar';

    column distance_content =>
        type is 'integer';

    column pace =>
        type is 'varchar';

    column start_time =>
        type is 'varchar',
        label is 'Start time';
        is distinct;

    column calories =>
        type is 'integer',
        label is 'Calories';

    column duration =>
        type is 'integer';
    
    column steps_runbegin =>
        type is 'integer';

    column steps_runend =>
        type is 'integer';

    column steps_walkbegin =>
        type is 'integer';

    column steps_walkend =>
        type is 'integer';

    column name =>
        type is 'varchar',
        label is 'Name',
        hint is 'The name of the track you\'ve run';

    column interval_value =>
        type is 'integer';

    column interval_datatype =>
        type is 'varchar';

    column interval_unit =>
        type is 'varchar';

    column interval_type =>
        type is 'varchar';

    column goal_unit =>
        type is 'varchar';

    column goal_value =>
        type is 'varchar';

    column goal_type =>
        type is 'varchar';

    column user_info_calibration =>
        type is 'varchar';

    column user_info_weight =>
        type is 'varchar';

    # TODO - 
    # this is actually a foreign key to the user table, how to mark it as
    # one?
    column user_info_empedid =>
        type is 'varchar';

    column user_info_device =>
        type is 'varchar';

    column chart =>
        type is 'blob';

    column software_calibration =>
        type is 'varchar';
};

sub get_calibrated_distance {
    my $self = shift;
    if ($self->software_calibration) {
        return $self->software_calibration * $self->distance_content;
    }
    else {
        return $self->distance_content;
    }
}

sub get_calibrated_calories {
    my $self = shift;
    if ($self->software_calibration) {
        return $self->software_calibration * $self->calories;
    }
    else {
        return $self->calories;
    }
}

sub new_from_XML {
    my $self     = shift;
    my $filename = shift;

    Jifty->log->debug('new_from_XML filename: ' . $filename);

    my $xs = XML::Simple->new(
        KeyAttr       => [],
    );
    my $xml = $xs->XMLin($filename);
    Jifty->log->debug('parsed XML: ' . Dumper $xml);

    my $workout = KratosOrange::Model::Workout->new();
    my ($id) = $workout->create();

    $self->__set_fields_from_xml($workout, $xml);
    $self->__create_waypoints_from_xml($xml, $id);
    # TODO - create chart from waypoints

    return 1;
}

sub __create_waypoints_from_xml {
    my $self = shift;
    my $xml  = shift;
    my $id   = shift;

    KratosOrange::Model::WayPoint->new_from_extended_data(
        $xml->{extendedDataList}->{extendedData}->{content},
        $id,
    );
}

sub __set_fields_from_xml {
    my $self    = shift;
    my $workout = shift;
    my $xml     = shift;

    $workout->set_start_time(        $xml->{startTime}                        );
    $workout->set_user_info_empedid( $xml->{userInfo}->{empedID}              );

    $workout->set_distance_string(   $xml->{runSummary}->{distanceString}     );
    $workout->set_duration_string(   $xml->{runSummary}->{durationString}     );
    $workout->set_distance_unit(     $xml->{runSummary}->{distance}->{unit}   );
    $workout->set_distance_content(  $xml->{runSummary}->{distance}->{content});
    $workout->set_pace(              $xml->{runSummary}->{pace}               );
    $workout->set_calories(          $xml->{runSummary}->{calories}           );
    $workout->set_duration(          $xml->{runSummary}->{duration}           );

    $workout->set_steps_runbegin(
        $xml->{runSummary}->{stepCounts}->{runBegin},
    );
    $workout->set_steps_runend(
        $xml->{runSummary}->{stepCounts}->{runEnd},
    );
    $workout->set_steps_walkbegin(
        $xml->{walkSummary}->{stepCounts}->{walkBegin},
    );
    $workout->set_steps_walkend(
        $xml->{walkSummary}->{stepCounts}->{walkEnd},
    );

    $workout->set_name(
        $xml->{walkSummary}->{workoutName}
    );

    $workout->set_interval_value(
        $xml->{extendedDataList}->{extendedData}->{intervalValue}
    );
    $workout->set_interval_datatype(
        $xml->{extendedDataList}->{extendedData}->{dataType}
    );
    $workout->set_interval_unit(
        $xml->{extendedDataList}->{extendedData}->{intervalUnit}
    );
    $workout->set_interval_type(
        $xml->{extendedDataList}->{extendedData}->{intervalType}
    );

    $workout->set_goal_unit( $xml->{goal}->{unit} );
    $workout->set_goal_value( $xml->{goal}->{value} );
    $workout->set_goal_type( $xml->{goal}->{type} );

    $workout->set_user_info_calibration( $xml->{userInfo}->{calibration} );
    $workout->set_user_info_weight(      $xml->{userInfo}->{weight}      );
    $workout->set_user_info_device(      $xml->{userInfo}->{device}      );

    return 1;
}

1;

