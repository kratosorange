package KratosOrange::View;

use strict;
use warnings;
use utf8;

use Jifty::View::Declare -base;
use Data::Dumper;
use Time::Elapsed qw( elapsed );

template 'summary' => page {
    h1 { 'Summary' }
    my $workouts = KratosOrange::Model::WorkoutCollection->new();
    $workouts->limit(
        column => 'user_info_empedid',
        value => Jifty->web->current_user->user_object->empedid,
    );
    $workouts->order_by({
        column => 'start_time',
        order  => 'DESC',
    });
    my $nr_of_workouts = $workouts->count();
    my $calories = 0;
    while (my $wo = $workouts->next()) {
        $calories += $wo->get_calibrated_calories();
    }
    $calories = sprintf("%d", $calories);
    $workouts->goto_first_item;

    my $distance = 0;
    while (my $wo = $workouts->next()) {
        $distance += $wo->get_calibrated_distance();
    }
    $distance = sprintf("%.2f", $distance);
    # TODO - this assumes that all distance units are of the same type
    my $distance_unit = $workouts->first->distance_unit();

    $workouts->goto_first_item;
    my $duration_s = 0;
    while (my $wo = $workouts->next()) {
        $duration_s += $wo->duration();
    }
    $duration_s = int($duration_s / 1000); # duration is actually given in ms
    my $duration_string = elapsed($duration_s);
    my $current_weight = $workouts->first->user_info_weight();

    # end of code, let the template begin ...
    # TODO - fancier design
    p {
        "In $nr_of_workouts runs, you have run $distance $distance_unit."
    }
    p {
        "You have run $duration_s seconds in total. This equals about $duration_string on the run."
    }
    p {
        "You have burned $calories calories while running. Your last logged weight ist $current_weight."
    }
};

template 'import_synched' => page {
    h1 { 'Import already synchronised runs' }
    p { 'Welcome to Kratos Orange. Do you want to import already synchronised data into this application?' }
    div { show 'import_form' }
};

private template 'import_form' => sub {
    form {
        my $action = new_action ( class => 'ImportSynched' );
        render_action( $action => [ 'import' ]);
        # form_next_page( '/' );
        form_submit( );
        #form_return( label => 'Do not import' );
    }
};

template 'preferences' => page {
    h1 { 'Preferences' }
    div { show 'pref_form' }
};

private template pref_form => sub {
    form {
            my $user_record = Jifty->web->current_user->user_object();
            my $action = new_action(
                class   => 'UpdateUser',
                moniker => 'update_user',
                record  => $user_record,
            );
            if ($user_record->empedid() eq '') {
                # probably a new user who has never set the empedid before
                # look for possible emped IDs in the filesystem
                my @emped_ids = $user_record->get_emped_ids_from_fs();
                if (! scalar @emped_ids) {
                    p {
                        'Welcome to Kratos Orange. The first thing that we need to do is to figure out your sensor ID. If you have your iPod connected, we will try to find it via the filesystem. Please connect your iPod and reload this page. If you know the ID by heart (you geek :-), you can also enter it manually.'
                    }
                }
                elsif (scalar @emped_ids == 1) {
                    # if only one is available, we can just set it and
                    # return to /
                    $user_record->set_empedid($emped_ids[0]);
                    # TODO - return
                    redirect('/');
                }
                else {
                    # TODO - provide user with a selection dialog
                }
            }
            render_action( $action => [ 'empedid' ] );
            # TODO - figure out how to do a return
            form_submit();
    }
};

template 'chart' => sub {
    my $workout_id = get('workout_id');
    # TODO - possibly refactor to model completely
    my $workout = KratosOrange::Model::Workout->load($workout_id);
    if (! $workout->chart) {
        # chart does not exist yet, we have to render it and save it
        # the database to cache it
        my $chart =
            KratosOrange::Model::WayPoint->chart_from_workout($workout_id);
        $workout->set_chart($chart);
    }
    # output chart with appropriate header
    Jifty->handler->apache->content_type('image/png');
    Jifty->web->out($workout->chart);
};

# TODO - calibrated pace
# Pace: ", $workout->pace
# TODO - more and fancier output
template 'workout' => page {
    my $workout_id = get('workout_id');
    my $workout = KratosOrange::Model::Workout->load($workout_id);
    my $p_string = "Workout started at " . $workout->start_time() . ". Duration: ". $workout->duration_string() . ". Distance: " . $workout->get_calibrated_distance(). " ". $workout->distance_unit() . ".";
    h1 { $workout->name ? $workout->name : $workout->start_time };
    p {
        $p_string
    };
    img { attr { src => "/workout/$workout_id/chart" }; };
};

template 'workout_edit' => page {
    my $workout_id = get('workout_id');
    my $workout = KratosOrange::Model::Workout->load($workout_id);
    h1 { 'Edit workout' }
    div {
        form {
            my $action = new_action ( class => 'UpdateWorkout' );
            $action->hidden('id' => $workout_id);
            render_action(
                $action => [ 'name', 'id' ],
            );
            form_submit( );
        }
    }
};

template 'workouts' => page {
    h1 { 'My workouts' }
    my $workouts = KratosOrange::Model::WorkoutCollection->new();
    $workouts->limit(
        column => 'user_info_empedid',
        value => Jifty->web->current_user->user_object->empedid,
    );
    $workouts->order_by({
        column => 'start_time',
        order  => 'DESC',
    });
    table {
        row {
            th { 'Date' },
            th { 'Name' },
            th { 'Duration' },
            th { 'Distance' },
            th { 'Pace' },
            th { 'Actions'},
        };
        while (my $wo = $workouts->next()) {
            row {
                # TODO - format more nicely
                my $wo_id = $wo->id;
                my $view_link = Jifty->web->link(
                    url   => "/workout/$wo_id",
                    label => $wo->start_time,
                );
                my $edit_link = Jifty->web->link(
                    url       => "/workout/$wo_id/edit",
                    label     => 'Edit',
                    # as_button => 1,
                );
                cell { $view_link },
                cell { $wo->name ? $wo->name : 'No name' }, # TODO - input
                cell { $wo->duration_string },
                cell { $wo->get_calibrated_distance . ' ' . $wo->distance_unit },
                cell { $wo->pace }, # TODO - convert to km/h
                cell { $edit_link }, # TODO - convert to km/h
            },
        }
    }
};
1;

