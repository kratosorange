Database layout:

snapshots
    workout_id
    type
    distance
    pace
    duration
    event

workout
    distance_string
    duration_string
    distance_unit
    distance_content
    pace
    start_time
    calories
    duration
    steps_runbegin
    steps_runend
    steps_walkbegin
    steps_walkend
    name
    interval_value
    interval_datatype
    interval_unit
    interval_type
    goal_unit
    goal_value
    goal_type
    user_info_calibration
    user_info_weight
    user_info_empedid
    user_info_device
    chart -> base64 encodedes png

datapoint
    workout_id
    nr (fortlaufend)
    distance
