#!/usr/bin/perl 

use strict;
use warnings;

use XML::Simple;
use Data::Dumper;

my $xs = XML::Simple->new(
    KeyAttr       => [],
    KeepRoot      => 1,
);

my $xml = $xs->XMLin($ARGV[0]);

print Dumper $xml;
